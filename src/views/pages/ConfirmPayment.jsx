/*!

=========================================================
* Argon Dashboard PRO React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";
// react library for routing
import { Link, useParams } from "react-router-dom";
// reactstrap components
import classnames from "classnames";
import {
    Button,
    Card,
    CardBody,
    Col,
    Container,
    NavItem,
    NavLink,
    Nav,
    Progress,
    TabContent,
    TabPane,
    Row,
    Form
} from "reactstrap";

import qoreContext from "./../../qoreContext";

function ConfirmPayment() {
    const params = useParams()
    const {data: donation, status, error} = qoreContext.view("allDonation").useGetRow(params.donationId, { networkPolicy: "network-and-cache" });
    const {data: allRekening} = qoreContext.view("allRekening").useListRow({ networkPolicy: "network-and-cache", polling: 5000 })
    const numberFormat = new Intl.NumberFormat("id-ID", {
        style: "currency",
        currency: "IDR",
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    })
    
    const { updateRow, updateStatus } = qoreContext.view("allDonation").useUpdateRow();
    const updatePayment = async (e) => {
        e.preventDefault();
        const data = {
            state: 'confirm',
            dateTransfer: new Date()
        }
        await updateRow(params.donationId, { ...data });
        window.location.href = '/home/finished/'+params.donationId;
    }

    return (
    <>
        <div className="main-content bg-secondary pb-4">
            <section className="section pt-5 pb-5">
                <Container>
                    <Row sm="12" className="pb-2 text-center">
                        <Col>
                            <h1 className="display-3">
                                <strong>Konfirmasi Pembayaran</strong>
                            </h1>
                        </Col>
                    </Row>
                    <Row sm="12">
                        {allRekening.length > 0 ? 
                        <Col className="mb-3" >
                            <Card>
                                <CardBody>
                                    <Row className="mb-4 mt-2 align-items-center text-center">
                                        <Col md="12" sm="12">
                                            <span>
                                                <strong>Silahkan lakukan Transfer Manual sejumlah</strong>
                                            </span>
                                            <span>
                                                <h2 className="mb-1 display-4">
                                                    <strong>{numberFormat.format(donation ? donation.nominal : 0)}</strong>
                                                </h2>
                                            </span>
                                        </Col>
                                    </Row>
                                    <Row className="mb-4 mt-2 align-items-center text-center">
                                        <Col md="12" sm="12">
                                            <span>
                                                <strong>Menuju akun</strong>
                                            </span>
                                            <span>
                                                <h2 className="mb-1 display-3">
                                                    <strong>{allRekening[0].nomor}</strong>
                                                </h2>
                                            </span>
                                            <span>
                                            {allRekening[0].description}
                                            </span>  
                                        </Col>
                                    </Row>
                                    <Row className="mb-4 mt-2 align-items-center text-center">
                                        <Col md="12" sm="12">
                                            <span>
                                                <strong>Atas nama</strong>
                                            </span>
                                            <span>
                                                <h2 className="mb-1">
                                                    <strong>{allRekening[0].name}</strong>
                                                </h2>
                                            </span>
                                        </Col>
                                    </Row>
                                    <Row className="mt-2 text-center">
                                        <Col md="12" sm="12">
                                            <span className="text-center">
                                                Lalu, silahkan upload bukti transfer disini
                                            </span> 
                                        </Col>
                                    </Row>
                                    <Row className="mt-2 mb-2 justify-content-md-center">
                                        <Col md="6" sm="12">
                                            <Form>
                                                <div className="custom-file">
                                                    <input
                                                        className="custom-file-input"
                                                        id="customFileLang"
                                                        lang="en"
                                                        type="file"
                                                    />
                                                    <label
                                                        className="custom-file-label"
                                                        htmlFor="customFileLang"
                                                    >
                                                        Select file
                                                    </label>
                                                </div>
                                            </Form>
                                        </Col>
                                    </Row>
                                </CardBody>
                            </Card>
                            <Row>
                                <Col>
                                    <Button className="btn-icon btn-3" block color="primary" onClick={(e) => updatePayment(e)}>
                                        <span className="btn-inner--text">Upload Bukti Transfer</span>
                                    </Button>
                                </Col>
                            </Row>
                        </Col>
                        : 'Rekening tidak ditemukan' }
                    </Row>
                </Container>
            </section>
        </div>
    </>
  );
}

export default ConfirmPayment;
