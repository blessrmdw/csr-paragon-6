/*!

=========================================================
* Argon Dashboard PRO React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React, { useState } from "react";
// react library for routing
import { Link, useParams } from "react-router-dom";
// reactstrap components
import classnames from "classnames";
import {
    Button,
    Card,
    CardBody,
    Col,
    Container,
    NavItem,
    NavLink,
    Nav,
    Progress,
    TabContent,
    TabPane,
    Row,
    Input,
    InputGroup,
    FormGroup,
    InputGroupAddon,
    InputGroupText
} from "reactstrap";

import Loaders from 'components/Loaders/Loaders.js';
import qoreContext from "./../../qoreContext";
import NoEntry from "views/pages/NoEntry.jsx";

function Checkout() {
    const numberFormat = new Intl.NumberFormat("id-ID", {
        style: "currency",
        currency: "IDR",
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    })
    const [formValuesBuffer, setFormValuesBuffer] = useState({
        nominalTransfer: 0,
        donaturName: '',
        donaturEmail: '',
        donaturNumber: ''
    });
    const params = useParams()
    const {data: selectedProgram, status, error} = qoreContext.view("activity").useGetRow(params.campaignId, { networkPolicy: "network-and-cache" });
    const { insertRow, insertStatus } = qoreContext.view("allDonation").useInsertRow();
    if (status == 'loading') {
        return (<><Loaders/></>) 
    }
    else if (status == 'error') {
        return (<><NoEntry/></>)
    }

    const getTotalTransfer = () => {
        return formValuesBuffer.nominalTransfer;
    }

    const postPayment = async (e) => {
        e.preventDefault();
        const data = {
            nominal: formValuesBuffer.nominalTransfer,
            name: formValuesBuffer.donaturName,
            email: formValuesBuffer.donaturEmail,
            phone: formValuesBuffer.donaturNumber,
            campaign: params.campaignId,
            valid: false,
            state: 'waiting'
        }
        console.log(data)
        const result = await insertRow({...data});
        window.location.href = '/home/payment-confirmation/'+result.id;
    }

    return (
    <>
        <div className="main-content bg-secondary pb-4">
            <section className="section pt-5 pb-5">
                <Container>
                    <Row sm="12" className="pb-2">
                        <Col>
                            <h1 className="display-3">
                                <strong>Donasi</strong>
                            </h1>
                        </Col>
                    </Row>
                    <Row sm="12">
                        <Col xl="8" lg="8" md="6" className="mb-3" >
                            <Card>
                                <CardBody>
                                    <Row className="mb-5 mt-2 align-items-center">
                                        <Col xl="4" lg="4" md="4" sm="12">
                                            <img
                                                alt="..."
                                                className="img-fluid rounded"
                                                src={require("assets/img/theme/img-1-1000x600.jpg").default}
                                            />
                                        </Col>
                                        <Col xl="8" lg="8" md="8" sm="12">
                                            <span>
                                                <strong>Donasi untuk kegiatan</strong>
                                            </span>
                                            <span>
                                                <h2 className="mb-1 display-4">
                                                    <strong>{selectedProgram ? selectedProgram.name : '(Nama Campaign)'}</strong>
                                                </h2>
                                            </span>
                                            <span>
                                                Donasi akan diberikan kepada <strong>{selectedProgram ? selectedProgram.penerima : '(Penerima)'}</strong>
                                            </span>  
                                        </Col>
                                    </Row>
                                    <Row className="mt-5 mb-5 align-items-center">
                                        <Col>
                                            <Row>
                                                <div className="col ml-0">
                                                    <span>
                                                        <p className="mb-3">
                                                            <strong>Penanggung jawab</strong>
                                                        </p>
                                                    </span> 
                                                </div>
                                            </Row>
                                            { selectedProgram && selectedProgram.pic ? 
                                            (<>
                                            <Row>
                                                <Col className="col-auto">
                                                    <a
                                                    className="avatar rounded-circle"
                                                    onClick={(e) => e.preventDefault()}
                                                    >
                                                    <img
                                                        alt="..."
                                                        src={require("assets/img/theme/team-1.jpg").default}
                                                    />
                                                    </a>
                                                </Col>
                                                <div className="col ml-0">
                                                    <span>
                                                        <h2 className="mb-0">
                                                            <strong>{selectedProgram.pic.displayField}</strong>
                                                        </h2>
                                                    </span>
                                                    <span>
                                                    </span>  
                                                </div>
                                            </Row>
                                            </>) : '-'
                                            }
                                        </Col>
                                    </Row>
                                    <span>
                                        <strong>Masukkan Data dan Jumlah Donasi anda</strong>
                                    </span>
                                    <Row className="mt-2 align-items-center">
                                        <Col>
                                            <Row>
                                                <div className="col ml-0">
                                                    <FormGroup>
                                                        <span>
                                                            <p className="mb-3">
                                                                <strong>Nama</strong>
                                                            </p>
                                                        </span>
                                                        <InputGroup
                                                            className={classnames("input-group-merge")}
                                                        >
                                                            <Input
                                                                className="form-control"
                                                                type="text"
                                                                onChange={(e) => setFormValuesBuffer({
                                                                    donaturName: e.target.value,
                                                                    donaturEmail: formValuesBuffer.donaturEmail,
                                                                    donaturNumber: formValuesBuffer.donaturNumber,
                                                                    nominalTransfer: formValuesBuffer.nominalTransfer
                                                                })}
                                                            />
                                                        </InputGroup>
                                                    </FormGroup>
                                                </div>
                                                <div className="col ml-0">
                                                    <FormGroup>
                                                        <span>
                                                            <p className="mb-3">
                                                                <strong>No. Handphone</strong>
                                                            </p>
                                                        </span>
                                                        <InputGroup
                                                            className={classnames("input-group-merge")}
                                                        >
                                                            <InputGroupAddon addonType="prepend">
                                                                <InputGroupText>
                                                                    <i className="fas fa-phone" />
                                                                </InputGroupText>
                                                            </InputGroupAddon>
                                                            <Input
                                                                className="form-control"
                                                                type="text"
                                                                onChange={(e) => setFormValuesBuffer({
                                                                    donaturName: formValuesBuffer.donaturName,
                                                                    donaturEmail: formValuesBuffer.donaturEmail,
                                                                    donaturNumber: e.target.value,
                                                                    nominalTransfer: formValuesBuffer.nominalTransfer
                                                                })}
                                                            />
                                                        </InputGroup>
                                                    </FormGroup>
                                                </div>
                                            </Row>
                                            <Row>
                                                <div className="col ml-0">
                                                    <span>
                                                        <p className="mb-3">
                                                            <strong>Email</strong>
                                                        </p>
                                                    </span> 
                                                </div>
                                            </Row>
                                            <Row>
                                                <div className="col ml-0">
                                                    <FormGroup>
                                                        <InputGroup
                                                            className={classnames("input-group-merge")}
                                                        >
                                                            <InputGroupAddon addonType="prepend">
                                                                <InputGroupText>
                                                                    <small className="font-weight-bold">@</small>
                                                                </InputGroupText>
                                                            </InputGroupAddon>
                                                            <Input
                                                                className="form-control"
                                                                type="text"
                                                                onChange={(e) => setFormValuesBuffer({
                                                                    donaturName: formValuesBuffer.donaturName,
                                                                    donaturEmail: e.target.value,
                                                                    donaturNumber: formValuesBuffer.donaturNumber,
                                                                    nominalTransfer: formValuesBuffer.nominalTransfer
                                                                })}
                                                            />
                                                        </InputGroup>
                                                    </FormGroup>
                                                </div>
                                            </Row>
                                        </Col>
                                    </Row>
                                    <Row className="align-items-center">
                                        <Col>
                                            <Row>
                                                <div className="col ml-0">
                                                    <span>
                                                        <p className="mb-3">
                                                            <strong>Jumlah Donasi</strong>
                                                        </p>
                                                    </span> 
                                                </div>
                                            </Row>
                                            <Row>
                                                <div className="col ml-0">
                                                    <FormGroup>
                                                        <InputGroup
                                                            className={classnames("input-group-merge")}
                                                        >
                                                            <InputGroupAddon addonType="prepend">
                                                                <InputGroupText>
                                                                    <small className="font-weight-bold">IDR</small>
                                                                </InputGroupText>
                                                            </InputGroupAddon>
                                                            <Input
                                                                className="form-control-lg"
                                                                type="number"
                                                                onChange={(e) => setFormValuesBuffer({
                                                                    donaturName: formValuesBuffer.donaturName,
                                                                    donaturEmail: formValuesBuffer.donaturEmail,
                                                                    donaturNumber: formValuesBuffer.donaturNumber,
                                                                    nominalTransfer: e.target.value
                                                                })}
                                                            />
                                                        </InputGroup>
                                                    </FormGroup>
                                                </div>
                                            </Row>
                                        </Col>
                                    </Row>
                                </CardBody>
                            </Card>
                            <Row md="1">
                                <Col>
                                    <Button className="btn-icon" size="sm" href={`/home/single-campaign/${params.campaignId}`} color="primary">
                                        <span className="btn-inner--icon">
                                            <i className="ni ni-bold-left" />
                                        </span>
                                        <span className="btn-inner--text">Kembali ke Detail Campaign</span>
                                    </Button>
                                </Col>
                            </Row>
                        </Col>
                        <Col xl="4" lg="4" md="6" className="pl-4">
                            <Card>
                                <CardBody>
                                    <Row>
                                        <Col>
                                            <h3 name="amount-now" className="text-left">
                                                    Detail Pembayaran
                                            </h3>
                                            <div className="row mt-4 mb-2">
                                                <Col md="5" >
                                                    <span>
                                                        <p>
                                                            <strong>Donasi Anda</strong>
                                                        </p>
                                                    </span>
                                                </Col>
                                                <Col md="7">
                                                    <h3 className="text-right">
                                                        {numberFormat.format(formValuesBuffer.nominalTransfer)}
                                                    </h3> 
                                                </Col>     
                                            </div>
                                            <div className="row mt-2">
                                                <Col md="5">
                                                    <span>
                                                        <p>
                                                            <strong>Biaya Lain</strong>
                                                        </p>
                                                    </span>
                                                </Col>
                                                <Col md="7">
                                                    <h3 className="text-right">
                                                        {numberFormat.format(0)}
                                                    </h3> 
                                                </Col>     
                                            </div>
                                            <hr/>
                                            <div className="row mb-2">
                                                <Col md="5">
                                                    <span>
                                                        <h3>
                                                            <strong>TOTAL</strong>
                                                        </h3>
                                                    </span>
                                                </Col>
                                                <Col md="7">
                                                    <h3 className="text-right">
                                                        {numberFormat.format(getTotalTransfer())}
                                                    </h3> 
                                                </Col>     
                                            </div>
                                        </Col>
                                    </Row>
                                </CardBody>            
                            </Card>
                            <Row>
                                <Container>
                                    <p className="pb-2">
                                        <strong>
                                            Dengan klik <b>Lanjutkan</b>, saya telah membaca dan menerima <a href="#">Syarat dan Ketentuan</a>
                                        </strong>
                                    </p>
                                </Container>
                            </Row>
                            <Row>
                                <Col>
                                    <Button className="btn-icon btn-3" block color="primary" onClick={(e) => postPayment(e)}>
                                        <span className="btn-inner--text">Lanjutkan</span>
                                    </Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </section>
        </div>
    </>
  );
}

export default Checkout;
