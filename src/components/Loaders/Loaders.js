import React from "react";
import Loader from 'react-loader-spinner';

function Loaders() {
    return(
        <>
            <div id ="centerLoader" style={{position:"fixed", top:"50%", left:"50%"}}><Loader type="Circles" color="#00BFFF" height={80} width={80} style={{alignItems: "center", backgroundPosition: "center center"}}/></div>
        </>
    );
}

export default Loaders;